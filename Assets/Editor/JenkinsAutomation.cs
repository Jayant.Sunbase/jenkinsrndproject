using UnityEditor;
using System.Collections.Generic;
using UnityEditor.Build.Reporting;
using System.Security.Policy;
using UnityEngine;
using System;
using System.Drawing;
using Codice.Client.Commands;
using System.Security.Cryptography.X509Certificates;
using UnityEngine.Rendering;

public class JenkinsAutomation
{
    public static string buildName = "JenkinsTestBuild";
    public static string buildExtension = "";
    public static string outputPath = GetCurrentSystemPlatform();
    public static string outputPathWindows = "W:/Projects/SunbaseProjects/JenkinsRnDProject/JenkinsRnD/Builds/Automation/";
    public static string outputPathMacOS = "/Users/jayantsharma/UnityProjects/SunbaseProjects/JenkinsRnDProject/jenkinsrndproject/Builds/Automation/";

    const string androidBuildExtension = ".aab";
    const string androidDevBuildExtension = ".apk";
    const string iosBuildExtension = "";
    const string windowsBuildExtension = ".exe";
    const string macOSBuildExtension = ".app";

    const string ANDROID = "Android";
    const string ANDROID_DEV = "Android-Dev";
    const string IOS = "iOS";
    const string WINDOWS = "StandaloneWindows64";
    const string MACOS = "StandaloneOSX";
    const string ALL = "All";

    // Queue to store actions
    private static Queue<Action> actionQueue = new Queue<Action>();

    // Queue to store parameters
    private static Queue<BuildPlatformParameters> parameterQueue = new Queue<BuildPlatformParameters>();

    // Flag to check if queue is currently processing
    private static bool isProcessingQueue = false;

    #region  Scenes

    // Find all enabled scenes from Build Settings
    static string[] enabledScenes = FindEnabledEditorScenes();

    private static string[] FindEnabledEditorScenes()
    {

        List<string> EditorScenes = new List<string>();
        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
            if (scene.enabled)
                EditorScenes.Add(scene.path);

        return EditorScenes.ToArray();
    }

    #endregion

    #region  Parameter Parsing

    private class Args
    {
        public string platform = "";
    }

    public class BuildPlatformParameters
    {
        public BuildTargetGroup buildTargetGroup;
        public BuildTarget buildTarget;
        public string requestedPlatform;
    }

    private static Args FindArgs()
    {
        var returnValue = new Args();

        string[] args = Environment.GetCommandLineArgs();
        var execMethodArgPos = -1;
        bool allArgsFound = false;
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == "-executeMethod")
            {
                execMethodArgPos = i;
            }
            var realPos = execMethodArgPos == -1 ? -1 : i - execMethodArgPos - 2;
            if (realPos < 0)
                continue;

            if (realPos == 0)
                returnValue.platform = args[i];

            allArgsFound = true;
        }

        if (!allArgsFound)
            Console.WriteLine("[JenkinsBuild] Incorrect Parameters for -executeMethod Format: -executeMethod JenkinsBuild.BuildWindows64 <app name> <output dir>");

        return returnValue;
    }

    #endregion

    #region  Build

    public static void Build()
    {
        Console.WriteLine("output path value: " + outputPath);
        var args = FindArgs();
        string requestedPlatform = args.platform;
        Console.WriteLine("Recieved Argument: " + requestedPlatform);

        BuildTarget activeTarget = EditorUserBuildSettings.activeBuildTarget;

        if (activeTarget.ToString() != requestedPlatform)
        {
            if (requestedPlatform == ALL)
            {
                BuildAllPlatforms();
            }
            else
            {
                ExecuteBuild(ReturnBuildPlatformParameters(requestedPlatform));
            }
        }
        else
        {
            Console.WriteLine("Platform switching not required");
            ParseBuildData(requestedPlatform);
        }

        Console.WriteLine("[JenkinsBuild] Building:" + requestedPlatform);
    }

    public static void ExecuteBuild(BuildPlatformParameters buildPlatformParameters)
    {
        if (SwitchTargetPlatform(buildPlatformParameters.buildTargetGroup, buildPlatformParameters.buildTarget))
            ParseBuildData(buildPlatformParameters.requestedPlatform);
        else
            Console.WriteLine("Build Failed: " + buildPlatformParameters.requestedPlatform + " Switch Failed");
    }

    public static void ParseBuildData(string requestedPlatform)
    {
        outputPath += "/" + requestedPlatform + "/" + DateTime.Today.ToString("dd-MM-yyyy") + "/" + ReturnCurrentTime() + "/" + buildName;

        switch (requestedPlatform)
        {
            case ANDROID:
                {
                    BuildAndroid();
                    break;
                }
            case ANDROID_DEV:
                {
                    BuildAndroidDev();
                    break;
                }
            case IOS:
                {
                    BuildIOS();
                    break;
                }
            case WINDOWS:
                {
                    BuildWindows();
                    break;
                }
            case MACOS:
                {
                    BuildMacOS();
                    break;
                }
        }
    }

    public static void PublishBuild(BuildPlayerOptions buildPlayerOptions)
    {
        BuildReport buildReport = BuildPipeline.BuildPlayer(buildPlayerOptions);

        BuildSummary buildSummary = buildReport.summary;
        if (buildSummary.result == BuildResult.Succeeded)
        {
            Console.WriteLine("[JenkinsBuild] Build Success: Time:" + buildSummary.totalTime + " Size:" + buildSummary.totalSize + " bytes");

            // If queue isn't being processed, start processing
            if (!isProcessingQueue)
            {
                ProcessQueue();
            }
        }
        else
        {
            Console.WriteLine("[JenkinsBuild] Build Failed: Time:" + buildSummary.totalTime + " Total Errors:" + buildSummary.totalErrors);
        }
    }

    private static void OnBuildCompleted(BuildReport report)
    {
        // The build operation is finished
        if (report.summary.result == BuildResult.Succeeded)
        {
            Console.WriteLine("Build completed successfully!");
        }
        else
        {
            Console.WriteLine("Build failed!");
        }
    }

    #endregion

    #region  All Platforms Build

    public static void BuildAllPlatforms()
    {
        EnqueueAction(ExecuteBuild, ReturnBuildPlatformParameters(WINDOWS));
        EnqueueAction(ExecuteBuild, ReturnBuildPlatformParameters(ANDROID));
        EnqueueAction(ExecuteBuild, ReturnBuildPlatformParameters(ANDROID_DEV));
        EnqueueAction(ExecuteBuild, ReturnBuildPlatformParameters(MACOS));
        EnqueueAction(ExecuteBuild, ReturnBuildPlatformParameters(IOS));

        // If queue isn't being processed, start processing
        if (!isProcessingQueue)
        {
            StartProcessingQueue();
        }
    }

    // Start processing of the queue
    private static void StartProcessingQueue()
    {
        isProcessingQueue = true;
        ProcessQueue();
    }

    // Static method to add function to the queue
    public static void EnqueueAction(Action<BuildPlatformParameters> action, BuildPlatformParameters parameters)
    {
        actionQueue.Enqueue(() => action(parameters));
    }

    // Process the queue
    private static void ProcessQueue()
    {
        while (actionQueue.Count > 0)
        {
            // Dequeue the action
            Action action = actionQueue.Dequeue();

            // Invoke the action
            action.Invoke();
        }

        isProcessingQueue = false;
    }

    #endregion

    #region Android Builds

    public static void BuildAndroid()
    {
        buildExtension = androidBuildExtension;

        // Configure build options
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            scenes = enabledScenes,
            locationPathName = outputPath + buildExtension,
            target = BuildTarget.Android,
            options = BuildOptions.None
        };

        // Set keystore information (replace with your keystore details)
        PlayerSettings.Android.keystoreName = "path/to/your.keystore";
        PlayerSettings.Android.keystorePass = "your_keystore_password";
        PlayerSettings.Android.keyaliasName = "your_key_alias";
        PlayerSettings.Android.keyaliasPass = "your_key_alias_password";

        // Build AAB
        PublishBuild(buildPlayerOptions);
    }

    public static void BuildAndroidDev()
    {
        buildExtension = androidDevBuildExtension;

        // Configure build options for a development build without signing
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            scenes = enabledScenes,
            locationPathName = outputPath + buildExtension,
            target = BuildTarget.Android,
            options = BuildOptions.Development
        };

        // Build APK
        PublishBuild(buildPlayerOptions);
    }

    #endregion

    #region  IOS Build

    public static void BuildIOS()
    {
        buildExtension = iosBuildExtension;

        // Configure build options
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            scenes = enabledScenes,
            locationPathName = outputPath + buildExtension,
            target = BuildTarget.iOS,
            options = BuildOptions.None
        };

        // Build iOS Xcode project
        PublishBuild(buildPlayerOptions);

        // If you want to further automate the process and trigger Xcode build from Unity script,
        // you can use command line tools like xcodebuild or AppleScript to achieve that.
        // Here, we'll just notify the user that the Xcode project is ready for further processing.
        Console.WriteLine("iOS Xcode project built. Open the project in Xcode to continue.");
    }

    #endregion

    #region  Windows Build

    public static void BuildWindows()
    {
        buildExtension = windowsBuildExtension;

        // Configure build options
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            scenes = enabledScenes,
            locationPathName = outputPath + buildExtension,
            target = BuildTarget.StandaloneWindows,
            options = BuildOptions.None
        };

        // Build Windows application
        PublishBuild(buildPlayerOptions);
    }

    #endregion

    #region MacOS Build

    public static void BuildMacOS()
    {
        buildExtension = macOSBuildExtension;

        // Configure build options
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            scenes = enabledScenes,
            locationPathName = outputPath + buildExtension,
            target = BuildTarget.StandaloneOSX,
            options = BuildOptions.None
        };

        // Build macOS application
        PublishBuild(buildPlayerOptions);
    }

    #endregion

    #region  Value Returning Functions

    public static bool SwitchTargetPlatform(BuildTargetGroup buildTargetGroup, BuildTarget buildTarget)
    {
        bool switchResult = EditorUserBuildSettings.SwitchActiveBuildTarget(buildTargetGroup, buildTarget);

        if (switchResult)
        {
            Console.WriteLine("[JenkinsBuild] Successfully changed Build Target to: " + buildTarget.ToString());
        }
        else
        {
            Console.WriteLine("[JenkinsBuild] Unable to change Build Target to: " + buildTarget.ToString() + " Exiting...");
            return false;
        }
        return true;
    }

    public static string GetCurrentSystemPlatform()
    {
        string operatingSystem = SystemInfo.operatingSystem;
        if (operatingSystem.Contains("Windows"))
        {
            Console.WriteLine("Returning Platform is WindowsOS");

            return "W:/Projects/SunbaseProjects/JenkinsRnDProject/JenkinsRnD/Builds/Automation/";
        }
        else
        {
            Console.WriteLine("Returning Platform is MacOS");

            return "/Users/jayantsharma/UnityProjects/SunbaseProjects/JenkinsRnDProject/jenkinsrndproject/Builds/Automation/";
        }
    }

    public static string ReturnCurrentTime()
    {
        // Get the current system time
        DateTime currentTime = DateTime.Now;

        // Extract components of the current time
        int hour = currentTime.Hour;
        int minute = currentTime.Minute;

        /// Determine if it's AM or PM
        string am_pm = (hour < 12) ? "am" : "pm";

        // Convert to 12-hour format
        if (hour > 12)
            hour -= 12;
        else if (hour == 0) // If hour is 0, convert to 12
            hour = 12;

        // Create a string representing the current time
        string timeString = string.Format("{0}-{1:D2} {2}", hour, minute, am_pm);

        return timeString;
    }

    public static BuildPlatformParameters ReturnBuildPlatformParameters(string requestedPlatform)
    {
        BuildPlatformParameters buildPlatformParameters = new BuildPlatformParameters();

        switch (requestedPlatform)
        {
            case IOS:
                {
                    buildPlatformParameters.buildTargetGroup = BuildTargetGroup.iOS;
                    buildPlatformParameters.buildTarget = BuildTarget.iOS;
                    buildPlatformParameters.requestedPlatform = requestedPlatform;
                    return buildPlatformParameters;
                }
            case WINDOWS:
                {
                    buildPlatformParameters.buildTargetGroup = BuildTargetGroup.Standalone;
                    buildPlatformParameters.buildTarget = BuildTarget.StandaloneWindows64;
                    buildPlatformParameters.requestedPlatform = requestedPlatform;
                    return buildPlatformParameters;
                }
            case MACOS:
                {
                    buildPlatformParameters.buildTargetGroup = BuildTargetGroup.Standalone;
                    buildPlatformParameters.buildTarget = BuildTarget.StandaloneOSX;
                    buildPlatformParameters.requestedPlatform = requestedPlatform;
                    return buildPlatformParameters;
                }
            default:
                {
                    buildPlatformParameters.buildTargetGroup = BuildTargetGroup.Android;
                    buildPlatformParameters.buildTarget = BuildTarget.Android;
                    buildPlatformParameters.requestedPlatform = requestedPlatform;
                    return buildPlatformParameters;
                }
        }

        #endregion
    }
}